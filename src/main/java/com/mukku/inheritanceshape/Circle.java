/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.inheritanceshape;

/**
 *
 * @author iUser
 */
public class Circle extends Shape{
    private double r;
    private double pi = 22.0/7;
    
    public Circle(String name, double r) {
        super(name);
        this.r = r;
    }
    
    @Override
    public double calArea(){
        return pi*r*r;
    }

    
    public double getR() {
        return r;
    }

    public double getPi() {
        return pi;
    }
    
    @Override
    public void print(){
        if(r <= 0) System.out.println("Radius must more than 0!!");
        else System.out.println("Area of " + name + " : " + calArea());
    }
}
