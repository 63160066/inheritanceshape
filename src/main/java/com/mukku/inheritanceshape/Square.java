/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.inheritanceshape;

/**
 *
 * @author iUser
 */
public class Square extends Rectangle {
    private double side;
    
    public Square(String name, double side){
        super(name,side,side);
        this.side = side;
    }
    
    public void print(){
        if(side <= 0) System.out.println("Side must more than 0!!");
        else System.out.println("Area of " + name + " : " + calArea());
    }
}
