/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.inheritanceshape;

/**
 *
 * @author iUser
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    
    public Triangle(String name, double base, double height){
        super(name);
        this.base = base;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        return 0.5*base*height;
    }
    
    @Override
    public void print(){
        if(base <= 0 || height <= 0) System.out.println("Base and height must more than 0!!");
        else System.out.println("Area of " + name + " : " + calArea());
    }
}
