/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.inheritanceshape;

/**
 *
 * @author iUser
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape("Shape");
        shape.calArea();
        shape.print();
        
        Circle circle1 = new Circle("circle1", 3);
        circle1.calArea();
        circle1.print();
        
        Circle circle2 = new Circle("circle2", 4);
        circle2.calArea();
        circle2.print();
        
        Triangle triangle = new Triangle("triangle",3,4);
        triangle.calArea();
        triangle.print();
        
        Rectangle rectangle = new Rectangle("rectangle",3,4);
        rectangle.calArea();
        rectangle.print();
        
        Square square = new Square("square",2);
        square.calArea();
        square.print();
        
        Circle circle3 = new Circle("circle3", 0);
        circle3.calArea();
        circle3.print();
        
        Triangle triangle1 = new Triangle("triangle1",3,0);
        triangle1.calArea();
        triangle1.print();
        
        Rectangle rectangle1 = new Rectangle("rectangle1",0,4);
        rectangle1.calArea();
        rectangle1.print();
        
        Square square1 = new Square("square",0);
        square1.calArea();
        square1.print();
        
        System.out.println("|||||||");
        
        Shape[] shapes = {circle1, circle2, triangle, rectangle, square, 
            circle3, triangle1, rectangle1, square1};
        
        for(int i = 0; i < shapes.length; i++){
            shapes[i].calArea();
            shapes[i].print();
        }
        
    }
}
