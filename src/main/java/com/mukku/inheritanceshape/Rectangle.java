/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.inheritanceshape;

/**
 *
 * @author iUser
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    
    public Rectangle(String name, double width, double height){
        super(name);
        this.width = width;
        this.height = height;
    }
    
    public double calArea(){
        return width*height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    
    public void print(){
        if(width <= 0 || height <= 0) System.out.println("Width and height must more than 0!!");
        else System.out.println("Area of " + name + " : " + calArea());
    }
}
